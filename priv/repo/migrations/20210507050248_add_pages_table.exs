defmodule Rosacon.Repo.Migrations.AddPagesTable do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:pages, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :site_id, references(:sites, type: :uuid)
      add :status, :string, null: false, default: "draft"
      add :title, :string
      add :slug, :string, null: false
      add :description, :text
      add :content, :text
    end
  end
end
