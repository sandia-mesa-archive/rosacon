defmodule Rosacon.Repo.Migrations.UseFlakeIdForSites do
  use Ecto.Migration

  def change do
    # Doesn't really matter that we're dropping and re-creating this table currently just for making
    # the primary_key in the instances table a Flake ID. It's easier and we're still pretty early-on
    # in the development.
    drop_if_exists table(:sites)

    create_if_not_exists table(:sites, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string
      add :uri_host, :string, null: false
      add :admin_email_address, :string
      add :mail_sender_email_address, :string
      add :description, :string
      add :logo, :string
      add :favicon, :string
      add :home_page_id, :integer
      add :allow_user_blog_creation, :boolean, null: false, default: true
      add :open_user_registration, :boolean, null: false, default: true
      add :comments_enabled, :boolean, null: false, default: true
      add :email_confirmation_required, :boolean, null: false, default: false
      add :seo_overwrites, {:array, :map}, default: []

      timestamps()
    end
  end
end
