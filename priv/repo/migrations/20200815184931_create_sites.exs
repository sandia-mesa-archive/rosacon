defmodule Rosacon.Repo.Migrations.CreateSites do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:sites) do
      add(:site_id, :integer)
      add(:name, :string)
      add(:uri_host, :string)
      add(:admin_email_address, :string)
      add(:mail_sender_email_address, :string)
      add(:description, :string)
      add(:logo, :string)
      add(:favicon, :string)
      add(:home_page_id, :integer)
      add(:has_blog, :boolean)
      add(:posts_page_id, :integer)
      add(:posts_per_blog_list_page, :boolean)
      add(:open_user_registration, :boolean)
      add(:comments_enabled, :boolean)
      add(:email_confirmation_required, :boolean)
      add(:categories, {:array, :map}, default: [])
      add(:seo_overwrites, {:array, :map}, default: [])

      timestamps()
    end
  end
end
