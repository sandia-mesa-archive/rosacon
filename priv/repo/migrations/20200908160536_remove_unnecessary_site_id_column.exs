defmodule Rosacon.Repo.Migrations.RemoveUnnecessarySiteIdColumn do
  use Ecto.Migration

  def change do
    alter table(:sites) do
      remove_if_exists(:site_id, :integer)
    end
  end
end
