defmodule Rosacon.Repo.Migrations.ChangeBlogModelForSites do
  use Ecto.Migration

  def change do
    alter table(:sites) do
      remove_if_exists(:has_blog, :boolean)
      remove_if_exists(:posts_page_id, :integer)
      remove_if_exists(:posts_per_blog_list_page, :boolean)
      add_if_not_exists(:allow_user_blog_creation, :boolean)
    end
  end
end
