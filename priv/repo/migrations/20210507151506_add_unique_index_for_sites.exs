defmodule Rosacon.Repo.Migrations.AddUniqueIndexForSites do
  use Ecto.Migration

  def change do
    create unique_index(:sites, [:uri_host])
  end
end
