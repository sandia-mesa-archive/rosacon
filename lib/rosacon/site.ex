defmodule Rosacon.Site do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rosacon.Repo
  alias Rosacon.Site

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  # Regex for email addresses
  @email_regex ~r/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  schema "sites" do
    field :name, :string
    field :uri_host, :string, null: false
    field :admin_email_address, :string
    field :mail_sender_email_address, :string
    field :description, :string
    field :logo, :string
    field :favicon, :string
    field :home_page_id, :integer
    field :open_user_registration, :boolean, null: false, default: true
    field :allow_user_blog_creation, :boolean, null: false, default: true
    field :comments_enabled, :boolean, null: false, default: true
    field :email_confirmation_required, :boolean, null: false, default: false
    field :seo_overwrites, {:array, :map}, default: []

    timestamps()
  end

  def delete(site) do
    with {:ok, site} <- Repo.delete(site) do
      {:ok, site}
    end
  end

  def register(params) do
    changeset = register_changeset(params)

    with {:ok, site} <- Repo.insert(changeset) do
      {:ok, site}
    end
  end

  def get_by_uri_host(uri_host) do
    Repo.get_by(Site, uri_host: uri_host)
  end

  def set_uri_host(%Site{} = site, uri_host) do
    site
    |> change(uri_host: uri_host)
    |> Repo.update()
  end

  def set_name(%Site{} = site, name) do
    site
    |> change(name: name)
    |> Repo.update()
  end

  def set_comments_enabled(%Site{} = site, comments_enabled) do
    site
    |> change(comments_enabled: comments_enabled)
    |> Repo.update()
  end

  def set_description(%Site{} = site, description) do
    site
    |> change(description: description)
    |> Repo.update()
  end

  def set_open_user_registration(%Site{} = site, open_user_registration) do
    site
    |> change(open_user_registration: open_user_registration)
    |> Repo.update()
  end

  def set_allow_user_blog_creation(%Site{} = site, allow_user_blog_creation) do
    site
    |> change(allow_user_blog_creation: allow_user_blog_creation)
    |> Repo.update()
  end

  defp validation_changeset(changeset) do
    changeset
    |> validate_required([:name, :uri_host])
    |> validate_format(:admin_email_address, @email_regex)
    |> validate_format(:mail_sender_email_address, @email_regex)
    |> unique_constraint(:uri_host)
  end

  defp register_changeset(params) do
    %Site{}
    |> cast(params, [
      :name,
      :uri_host,
      :admin_email_address,
      :mail_sender_email_address,
      :description,
      :open_user_registration,
      :allow_user_blog_creation,
      :comments_enabled
    ])
    |> validation_changeset()
  end
end
