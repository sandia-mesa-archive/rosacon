defmodule Rosacon.Repo do
  use Ecto.Repo,
    otp_app: :rosacon,
    adapter: Ecto.Adapters.Postgres
end
