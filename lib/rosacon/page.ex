defmodule Rosacon.Page do
  use Ecto.Schema
  import Ecto.Changeset
  alias Rosacon.Repo
  alias Rosacon.Page
  alias Rosacon.Site

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "pages" do
    belongs_to(:site, Site, type: FlakeId.Ecto.CompatType)

    field :status, Ecto.Enum,
      values: [:draft, :published],
      null: false,
      default: :draft

    field :title, :string
    field :slug, :string, null: false
    field :description, :string
    field :content, :string

    timestamps()
  end

  def create(%Site{id: site_id}, slug) do
    %Page{}
    |> change(site_id: site_id, slug: slug)
    |> Repo.insert()
  end
end
