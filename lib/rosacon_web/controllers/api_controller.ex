defmodule RosaconWeb.ApiController do
  use RosaconWeb, :controller
  alias Rosacon.Site

  @doc "GET /api/rosacon/v1/site"
  def site(conn, _params) do
    with %Site{} = site <- Site.get_by_uri_host(conn.host) do
      json(conn, %{
        name: site.name,
        uri_host: site.uri_host,
        description: site.description,
        logo: site.logo,
        favicon: site.favicon,
        allow_user_blog_creation: site.allow_user_blog_creation,
        open_user_registration: site.open_user_registration,
        comments_enabled: site.comments_enabled
      })
    else
      _ ->
        json(conn, %{
          error: "Site not found"
        })
    end
  end
end
