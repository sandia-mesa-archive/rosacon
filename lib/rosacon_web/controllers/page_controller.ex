defmodule RosaconWeb.PageController do
  use RosaconWeb, :controller

  alias Rosacon.Site

  defp not_found(conn, header_title, message) do
    conn
    |> put_status(404)
    |> render("error.html", %{header_title: header_title, message: message})
  end

  def index(conn, _params) do
    network_name = Rosacon.Config.get([:instance, :network_name], "Rosacon")

    header_title =
      gettext("%{host} : %{network_name}", host: conn.host, network_name: network_name)

    case Site.get_by_uri_host(conn.host) do
      %Site{} = site ->
        render(conn, "index.html", %{
          site: site,
          header_title: site.name
        })

      _ ->
        not_found(conn, header_title, "Site not found.")
    end
  end
end
