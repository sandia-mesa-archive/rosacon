defmodule Mix.Tasks.Rosacon.Site do
  use Mix.Task
  import Mix.Rosacon
  
  alias Rosacon.Site

  @shortdoc "Generates a new site."
  def run(["new", name, uri_host | rest]) do
    {options, [], []} =
      OptionParser.parse(
        rest,
        strict: [
          name: :string,
          uri_host: :string,
          adminEmail: :string,
          senderEmail: :string,
          description: :string,
          openUserRegistration: :boolean,
          commentsEnabled: :boolean,
          allowUserBlogCreation: :boolean
        ]
      )

    name = Keyword.get(options, :name, name)
    uri_host = Keyword.get(options, :uri_host, uri_host)

    admin_email_address =
      Keyword.get(options, :adminEmail, Rosacon.Config.get([:instance, :default_admin_email]))

    mail_sender_email_address =
      Keyword.get(
        options,
        :senderEmail,
        Rosacon.Config.get([:instance, :default_mail_sender_email])
      )

    description = Keyword.get(options, :description, "")
    open_user_registration? = Keyword.get(options, :openUserRegistration, true)
    comments_enabled? = Keyword.get(options, :commentsEnabled, true)
    allow_user_blog_creation? = Keyword.get(options, :allowUserBlogCreation, true)

    shell_info("""
    A site will be created with the following information:
      - name: #{name}
      - uri_host: #{uri_host}
      - admin_email_address: #{admin_email_address}
      - mail_sender_email_address: #{mail_sender_email_address}
      - description: #{description}
      - open_user_registration: #{if(open_user_registration?, do: "true", else: "false")}
      - allow_user_blog_creation: #{if(allow_user_blog_creation?, do: "true", else: "false")}
      - comments_enabled: #{if(comments_enabled?, do: "true", else: "false")}
    """)

    proceed? = shell_yes?("Continue?")

    if proceed? do
      Mix.Task.run("app.start")

      params = %{
        name: name,
        uri_host: uri_host,
        admin_email_address: admin_email_address,
        mail_sender_email_address: mail_sender_email_address,
        description: description,
        open_user_registration: open_user_registration?,
        comments_enabled: comments_enabled?,
        allow_user_blog_creation: allow_user_blog_creation?
      }

      {:ok, _site} = Site.register(params)

      shell_info("Site #{name} created!")
    end
  end

  @shortdoc "Removes an existing site"
  def run(["rm", uri_host]) do
    Mix.Task.run("app.start")

    with %Site{} = site <- Site.get_by_uri_host(uri_host) do
      proceed? =
        shell_yes?(
          "Are you sure you want to delete the site at #{uri_host}? (THIS CANNOT BE UNDONE!!)"
        )

      if proceed? do
        {:ok, _site} = Site.delete(site)
        shell_info("Deleted site #{uri_host}")
      end
    else
      _ -> shell_error("No site #{uri_host}")
    end
  end

  @shortdoc "Changes a setting for an existing site"
  def run(["set", uri_host | rest]) do
    Mix.Task.run("app.start")

    {options, [], []} =
      OptionParser.parse(
        rest,
        strict: [
          name: :string,
          uriHost: :string,
          description: :string,
          openUserRegistration: :boolean,
          commentsEnabled: :boolean,
          allowUserBlogCreation: :boolean
        ]
      )

    with %Site{} = site <- Site.get_by_uri_host(uri_host) do
      site =
        case Keyword.get(options, :openUserRegistration) do
          nil -> site
          value -> set_open_user_registration(site, value)
        end

      site =
        case Keyword.get(options, :commentsEnabled) do
          nil -> site
          value -> set_comments_enabled(site, value)
        end

      site =
        case Keyword.get(options, :allowUserBlogCreation) do
          nil -> site
          value -> set_allow_user_blog_creation(site, value)
        end

      site =
        case Keyword.get(options, :name) do
          nil -> site
          value -> set_site_name(site, value)
        end

      site =
        case Keyword.get(options, :uriHost) do
          nil -> site
          _value = "" -> shell_error("Error: A site's URL cannot be blank!")
          value -> set_uri_host(site, value)
        end

      _site =
        case Keyword.get(options, :description) do
          nil -> site
          _value = "" -> set_site_description(site, nil)
          value -> set_site_description(site, value)
        end
    else
      _ ->
        shell_error("No site at #{uri_host}")
    end
  end

  @shortdoc "Changes the name of a site."
  defp set_site_name(site, value) do
    {:ok, site} =
      site
      |> Site.set_name(value)

    shell_info("Name of site at #{site.uri_host}: #{site.name}")
    site
  end

  @shortdoc "Changes the description of a site."
  defp set_site_description(site, value) do
    {:ok, site} =
      site
      |> Site.set_description(value)

    shell_info("Description of site at #{site.uri_host}: #{site.description}")
    site
  end

  @shortdoc "Changes the setting of a site regarding open user registration."
  defp set_open_user_registration(site, value) do
    {:ok, site} =
      site
      |> Site.set_open_user_registration(value)

    shell_info("Open registration status of #{site.uri_host}: #{site.open_user_registration}")
    site
  end

  @shortdoc "Changes the setting of a site regarding open user registration."
  defp set_comments_enabled(site, value) do
    {:ok, site} =
      site
      |> Site.set_comments_enabled(value)

    shell_info("Comments enabled status of #{site.uri_host}: #{site.comments_enabled}")
    site
  end

  @shortdoc "Changes the setting of a site regarding whether non-admin users can create blogs."
  defp set_allow_user_blog_creation(site, value) do
    {:ok, site} =
      site
      |> Site.set_allow_user_blog_creation(value)

    shell_info(
      "Allow user blog creation status of #{site.uri_host}: #{site.allow_user_blog_creation}"
    )

    site
  end

  @shortdoc "Changes the uri_host of an existing site."
  defp set_uri_host(site, value) do
    current_site = site.uri_host

    {:ok, site} =
      site
      |> Site.set_uri_host(value)

    shell_info("The site #{current_site} is now at #{site.uri_host}")
    site
  end
end
