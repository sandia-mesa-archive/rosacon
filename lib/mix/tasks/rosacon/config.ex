defmodule Mix.Tasks.Rosacon.Config do
  use Mix.Task
  import Mix.Rosacon

  @shortdoc "Manages Rosacon instance"

  def run(["gen" | rest]) do
    {options, [], []} =
      OptionParser.parse(
        rest,
        strict: [
          force: :boolean,
          output: :string,
          output_psql: :string,
          network_name: :string,
          default_admin_email: :string,
          default_mail_sender_email: :string,
          dbhost: :string,
          dbname: :string,
          dbuser: :string,
          dbpass: :string,
          listen_ip: :string,
          listen_port: :string
        ]
      )

    paths =
      [config_path, psql_path] = [
        Keyword.get(options, :output, "config/generated_config.exs"),
        Keyword.get(options, :output_psql, "config/setup_db.psql")
      ]

    will_overwrite = Enum.filter(paths, &File.exists?/1)
    proceed? = Enum.empty?(will_overwrite) or Keyword.get(options, :force, false)

    if proceed? do
      network_name =
        get_option(
          options,
          :network_name,
          "What will the name of the Rosacon network your site(s) runs on be? (e.g. Sandia Mesa Websites)",
          ":"
        )

      default_admin_email =
        get_option(
          options,
          :default_admin_email,
          "What's the admin email address you want to set for your sites by default?"
        )

      default_sender_email =
        get_option(
          options,
          :default_mail_sender_email,
          "What email address do you want to use for sending emails by default?",
          default_admin_email
        )

      dbhost = get_option(options, :dbhost, "What is the hostname of your database?", "localhost")

      dbname = get_option(options, :dbname, "What is the name of your database?", "rosacon")

      dbuser =
        get_option(
          options,
          :dbuser,
          "What is the user used to connect to your database?",
          "rosacon"
        )

      dbpass =
        get_option(
          options,
          :dbpass,
          "What is the password used to connect to your database?",
          :crypto.strong_rand_bytes(64) |> Base.encode64() |> binary_part(0, 64),
          "autogenerated"
        )

      listen_ip =
        get_option(
          options,
          :listen_ip,
          "What IP address will the app listen to (leave it if you are doing the default setup with nginx)?",
          "127.0.0.1"
        )

      listen_port =
        get_option(
          options,
          :listen_port,
          "What port will the app listen to (leave it if you are doing the default setup with nginx)?",
          9001
        )

      secret = :crypto.strong_rand_bytes(64) |> Base.encode64() |> binary_part(0, 64)
      signing_salt = :crypto.strong_rand_bytes(8) |> Base.encode64() |> binary_part(0, 8)

      template_dir = Application.app_dir(:rosacon, "priv") <> "/templates"

      result_config =
        EEx.eval_file(
          template_dir <> "/sample_config.eex",
          network_name: network_name,
          default_admin_email: default_admin_email,
          default_sender_email: default_sender_email,
          dbhost: dbhost,
          dbname: dbname,
          dbuser: dbuser,
          dbpass: dbpass,
          secret: secret,
          signing_salt: signing_salt,
          listen_ip: listen_ip,
          listen_port: listen_port
        )

      result_psql =
        EEx.eval_file(
          template_dir <> "/sample_psql.eex",
          dbname: dbname,
          dbuser: dbuser,
          dbpass: dbpass
        )

      shell_info("Writing config to #{config_path}...")
      File.write(config_path, result_config)
      shell_info("Writing the postgres script to #{psql_path}...")
      File.write(psql_path, result_psql)

      shell_info("\nAll files successfully written!")
    else
      shell_error(
        "The task would have overwritten the following files:\n" <>
          (Enum.map(paths, &"- #{&1}\n") |> Enum.join("")) <>
          "Rerun with `--force` to overwrite them."
      )
    end
  end
end
