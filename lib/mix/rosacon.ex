defmodule Mix.Rosacon do
  @doc "Outputs a shell message"
  def shell_info(message) do
    if mix_shell?(),
      do: Mix.shell().info(message),
      else: IO.puts(message)
  end

  @doc "Outputs a prompt for a y/n question."
  def shell_yes?(message) do
    if mix_shell?(),
      do: Mix.shell().yes?(message),
      else: shell_prompt(message, "Continue?") in ~w(Yn Y y)
  end

  @doc "A shell prompt function for when mix_shell is not available."
  def shell_prompt(prompt, defval \\ nil, defname \\ nil) do
    prompt_message = "#{prompt} [#{defname || defval}]"

    input =
      if mix_shell?(),
        do: Mix.shell().prompt(prompt_message),
        else: :io.get_line(prompt_message)

    case input do
      "\n" ->
        case defval do
          nil ->
            shell_prompt(prompt, defval, defname)

          defval ->
            defval
        end

      input ->
        String.trim(input)
    end
  end

  @doc "A function to output an error in the shell prompt."
  def shell_error(message) do
    if mix_shell?(),
      do: Mix.shell().error(message),
      else: IO.puts(:stderr, message)
  end

  @doc "A safe check to see if `Mix.shell/0` is available"
  def mix_shell?, do: :erlang.function_exported(Mix, :shell, 0)

  @doc "A function for getting options"
  def get_option(options, opt, prompt, defval \\ nil, defname \\ nil) do
    Keyword.get(options, opt) || shell_prompt(prompt, defval, defname)
  end
end
