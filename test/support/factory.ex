defmodule Rosacon.Factory do
  use ExMachina.Ecto, repo: Rosacon.Repo
  alias Rosacon.Site

  def site_factory do
    %Site{
      name: sequence(:name, &"Test Site #{&1}"),
      uri_host: sequence(:name, &"testsite-#{&1}.tld")
    }
  end
end
