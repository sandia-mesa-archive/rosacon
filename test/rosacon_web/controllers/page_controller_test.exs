defmodule RosaconWeb.PageControllerTest do
  use RosaconWeb.ConnCase
  import Rosacon.Factory

  test "GET / (when site exists)", %{conn: conn} do
    site = insert(:site)

    conn =
      conn
      |> Map.put(:host, site.uri_host)

    conn = get(conn, "/")
    assert html_response(conn, 200) =~ site.name
  end

  test "GET / (when site does not exist)", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 404) =~ "#{conn.host} : Rosacon"
  end
end
