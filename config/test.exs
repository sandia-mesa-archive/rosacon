use Mix.Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :rosacon, Rosacon.Repo,
  username: "postgres",
  password: "postgres",
  database: "rosacon_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :rosacon, RosaconWeb.Endpoint,
  http: [port: 4002],
  server: false,
  secret_key_base: "29CKSGRYBImW4eMr/XCyNgEF1qi6gAdiKag0UJ3wYWgOY2wd3RUBTuF6vWR2QkyF",
  signing_salt: "x2duiESH",
  live_view: [signing_salt: "+HUS0bAM"]

# Print only warnings and errors during test
config :logger, level: :warn
